# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponse

from rest_framework import viewsets
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.views import APIView
from .serializers import CartaSerializer
from .models import Carta


def index(request):
    return HttpResponse("Estas en el index de parejas.")

class CartaViewSet(viewsets.ModelViewSet):
    
    serializer_class = CartaSerializer
    
    def get_queryset(self):
        
        parejas = self.request.query_params.get('parejas')
        if parejas is not None:
            parejas = int(parejas)
            return Carta.objects.all()[:parejas]
        return Carta.objects.all()

class ContarCartasView(viewsets.ViewSet):    

    def list(self, request, format=None):
        conteo_cartas = Carta.objects.count()
        contenido = {'conteo_cartas': conteo_cartas}
        return Response(contenido)
        
